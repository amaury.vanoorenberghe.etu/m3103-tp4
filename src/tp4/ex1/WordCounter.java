package tp4.ex1;

import java.util.regex.Pattern;
import java.util.Map;
import java.util.HashMap;

public class WordCounter {
	private static final String WHITESPACE = "\\p{Blank}|\\p{Space}|\s"; 
	private static final String REGEX = WHITESPACE + "|\\p{Punct}|’";
	private static final Pattern PATTERN = Pattern.compile(REGEX);
	
	public static Map<String, Integer> countAll(String input) {
		String[] words = PATTERN.split(input.toLowerCase());
		
		Map<String, Integer> wordCount = new HashMap<String, Integer>();
		
		for (String word : words) {
			if (word.length() > 0) {
				Integer count = wordCount.get(word);
				wordCount.put(word, count == null ? 1 : count + 1);				
			}
		}
		
		return wordCount;
	}
}
