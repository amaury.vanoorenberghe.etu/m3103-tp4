package tp4.ex1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class Resource {
	public static String readtext(String resourceName) {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(Resource.class.getResourceAsStream(resourceName), StandardCharsets.UTF_8))) {
			StringBuilder sb = new StringBuilder();
			
			if (reader.ready()) {
				sb.append(reader.readLine());
			}
			
			while (reader.ready()) {
				sb.append(String.format("\n%s", reader.readLine()));
			}
			
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
