package tp4.ex1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) {
		String file = Resource.readtext("file.txt");
		Map<String, Integer> words = WordCounter.countAll(file);
		
		List<Map.Entry<String, Integer>> entries = new ArrayList<>(words.entrySet());
		
		Comparator<Map.Entry<String, Integer>> entryComparator = new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
				return Integer.compare(b.getValue(), a.getValue());
			};
		};
		
		Collections.sort(entries, entryComparator);

		for (Map.Entry<String, Integer> entry : entries.stream().limit(5).collect(Collectors.toList())) {
			printCount(words, entry.getKey());
		}
	}
	
	private static void printCount(Map<String, Integer> words, String word) {
		System.out.println(String.format("%s : %d", word, words.get(word)));
	}
	
	@SuppressWarnings("unused")
	private static void printContains(Map<String, Integer> words, String word) {
		for (Map.Entry<String, Integer> entry : words.entrySet()) {
			if (entry.getKey().contains(word)) {
				System.out.println(entry);
			}
		}
	}
}
