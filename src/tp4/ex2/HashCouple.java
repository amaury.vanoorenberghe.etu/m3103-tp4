package tp4.ex2;

import java.util.AbstractMap;

public class HashCouple<K, V> extends AbstractMap.SimpleEntry<K, V> {
	private static final long serialVersionUID = 1L;

	public HashCouple(K key, V value) {
		super(key, value);
	}
	
	@Override
	public int hashCode() {
		return getKey().hashCode();
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		if (!super.equals(obj)) {
			return false;
		}
		
		HashCouple<K, V> other = (HashCouple<K, V>)obj;
		
		if (!getKey().equals(other.getKey())) {
			return false;
		}
		
		if (!getValue().equals(other.getValue())) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return String.format("%s=%d", getKey(), getValue());
	}
}
