package tp4.ex2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public class Chainage<K, V> implements HashTable<K, V> {
	public static final int DEFAULT_SIZE = 16;
	private LinkedList<HashCouple<K, V>>[] table;
	private int currentSize = 0;
	
	public Chainage() {
		this(DEFAULT_SIZE);
	}

	public Chainage(int capacity) {
		table = new LinkedList[capacity];

		for (int i = 0; i < capacity; ++i) {
			table[i] = new LinkedList<HashCouple<K, V>>();
		}
	}

	@Override
	public V put(K key, V value) {
		HashCouple<K, V> c = find(key);
		
		if (c == null) {
			final int hash = getHash(key);
			c = new HashCouple<K, V>(key, value);
			table[hash].add(c);
			++currentSize;
			return null;
		} else {
			V old = c.getValue();
			c.setValue(value);
			return old;
		}
	}

	@Override
	public V get(K key) {
		final HashCouple<K, V> c = find(key);
		return c != null ? c.getValue() : null;
	}

	@Override
	public V remove(K key) {
		final int hash = getHash(key);
		
		final HashCouple<K, V> c = find(key);
		
		if (c != null) {
			table[hash].remove(c);
			--currentSize;
			return c.getValue();
		}

		return null;
	}

	@Override
	public boolean contains(K key) {
		return find(key) != null;
	}

	public void clear() {
		for (LinkedList<HashCouple<K, V>> list : table) {
			list.clear();
		}
		
		currentSize = 0;
	}

	@Override
	public int size() {
		return currentSize;
	}

	private HashCouple<K, V> find(K key) {
		final int hash = getHash(key);
		
		Iterator<HashCouple<K, V>> it = table[hash].iterator();
		
		HashCouple<K, V> c = null;
		while (it.hasNext() && !(c = it.next()).getKey().equals(key));
		
		return (c != null && c.getKey().equals(key)) ? c : null;
	}
	
	private int getHash(K key) {
		return Math.abs(key.hashCode()) % table.length;
	}
	
	@Override
	public String toString() {
		String items = Arrays
		.stream(table)
		.filter(list -> !list.isEmpty())
		.map(list -> list
			.stream()
			.map(item -> item.toString())
			.collect(Collectors.joining(", ")))
		.collect(Collectors.joining(", "));
		
		return String.format("[%s]", items);
	}
}
