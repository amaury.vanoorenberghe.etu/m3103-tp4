package tp4.ex2;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Adressage<K, V> implements HashTable<K, V> {
	private static final int NOT_FOUND = -1;
	public static final int DEFAULT_SIZE = 2048;

	private HashCouple<K, V>[] table;
	private int currentSize = 0;
	
	public Adressage() {
		this(DEFAULT_SIZE);
	}
	
	@SuppressWarnings("unchecked")
	public Adressage(int capacity) {
		table = new HashCouple[capacity];
	}

	@Override
	public V get(K key) {
		final int idx = find(key);
		
		if (idx != NOT_FOUND) {
			return table[idx].getValue();
		}
		
		return null;
	}

	@Override
	public V put(K key, V value) {
		final int hash = getHash(key);
		
		for (int idx = hash, cpt = 0; cpt < table.length; idx = next(idx), ++cpt) {
			if (table[idx] == null) {
				table[idx] = new HashCouple<K, V>(key, value);
				++currentSize;
				return null;
			} else if (table[idx].getKey().equals(key)) {
				V oldVal = table[idx].getValue();
				table[idx].setValue(value);
				return oldVal;
			}
		}
		
		throw new IllegalStateException();
	}

	@Override
	public V remove(K key) {
		final int idx = find(key);
		
		if (idx != NOT_FOUND) {
			final HashCouple<K, V> c = table[idx];

			table[idx] = null;
			--currentSize;
			return c.getValue();
		}
		
		return null;
	}

	@Override
	public boolean contains(K key) {
		return find(key) != NOT_FOUND;
	}

	@Override
	public void clear() {
		currentSize = 0;
		Arrays.fill(table, (HashCouple<K, V>)null);
	}

	@Override
	public int size() {
		return currentSize;
	}
	
	private int find(K key) {
		final int hash = getHash(key);
		
		int idx = hash, loop = 0;
		
		HashCouple<K, V> c = null;
		
		// TODO: Optimiser en transformant en while si possible
		for (;loop < table.length; ++loop) {
			c = table[idx];
			
			if (c != null && key.equals(c.getKey())) {
				return idx;
			}
			
			idx = next(idx);
		}
		
		return NOT_FOUND;
	}

	private int next(int value) {
		int result = value + 1;
		
		if (result >= table.length) {
			result = 0;
		}
		
		return result;
	}
	
	private int getHash(K key) {	
		return Math.abs(key.hashCode()) % table.length;
	}
	
	@Override
	public String toString() {
		String items = Arrays
		.stream(table)
		.filter(item -> item != null)
		.map(item -> item.toString())
		.collect(Collectors.joining(", "));
		
		return String.format("[%s]", items);
	}
}
