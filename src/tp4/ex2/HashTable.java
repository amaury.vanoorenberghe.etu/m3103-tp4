package tp4.ex2;

public interface HashTable<K, V> {
	public V get(K key);
	public V put(K key, V value);
	public V remove(K key);
	public boolean contains(K key);
	public void clear();
	public int size();
}
