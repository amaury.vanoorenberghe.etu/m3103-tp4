package tp4.ex2;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class HashTableTest {
	protected static Stream<Arguments> arguments() {
		return Stream
		.of(
			Arguments.of(new Chainage<String, Integer>()),
			Arguments.of(new Adressage<String, Integer>())
		);
	}

	@ParameterizedTest
	@MethodSource("arguments")
	protected void testToString(HashTable<String, Integer> table) {
		table.put("def", 2);
		table.put("ghi", 3);
		table.put("abc", 1);

		assertEquals("[abc=1, def=2, ghi=3]", table.toString());
	}

	@ParameterizedTest
	@MethodSource("arguments")
	protected void testPut(HashTable<String, Integer> table) {
		assertEquals(null, table.put("abc", 1));
		assertEquals(1, table.put("abc", 2));
		assertEquals(null, table.put("def", 8));
		assertEquals(8, table.put("def", 4));
		assertEquals(2, table.put("abc", 5));
	}

	@ParameterizedTest
	@MethodSource("arguments")
	protected void testRemove(HashTable<String, Integer> table) {
		table.put("abc", 123);
		table.put("def", 456);
		table.put("ghi", 789);

		assertEquals(null, table.remove("jkl"));
		assertEquals(123, table.remove("abc"));
		assertEquals(null, table.remove("abc"));
	}

	@ParameterizedTest
	@MethodSource("arguments")
	protected void testGet(HashTable<String, Integer> table) {
		table.put("abc", 123);
		table.put("def", 456);
		table.put("ghi", 789);

		assertEquals(null, table.get("jkl"));
		assertEquals(123, table.get("abc"));
		assertEquals(456, table.get("def"));
		assertEquals(789, table.get("ghi"));

	}

	@ParameterizedTest
	@MethodSource("arguments")
	protected void testSize(HashTable<String, Integer> table) {
		assertEquals(0, table.size());

		table.put("abc", 100);
		assertEquals(1, table.size());

		table.put("def", 200);
		assertEquals(2, table.size());

		table.remove("def");
		assertEquals(1, table.size());

		table.put("abc", 300);
		assertEquals(1, table.size());

		table.clear();
		assertEquals(0, table.size());
	}

	@ParameterizedTest
	@MethodSource("arguments")
	protected void testClear(HashTable<String, Integer> table) {
		assertEquals(0, table.size());

		table.put("a", 1);
		table.put("b", 2);
		table.put("c", 3);
		table.put("d", 4);

		assertEquals(4, table.size());

		table.clear();

		assertEquals(0, table.size());
	}

	@ParameterizedTest
	@MethodSource("arguments")
	protected void testContains(HashTable<String, Integer> table) {
		assertFalse(table.contains("abc"));

		table.put("abc", 100);
		assertTrue(table.contains("abc"));

		table.put("abc", 200);
		assertTrue(table.contains("abc"));

		table.remove("abc");
		assertFalse(table.contains("abc"));

		table.put("def", 300);
		assertTrue(table.contains("def"));

		table.clear();
		assertFalse(table.contains("def"));
	}
	
	// TODO: Ajouter tests pour clés qui ont des conflits dans Adressage
	// TODO: Ajouter tests en cas d'Adressage plein (ajout impossible -> IllegalStateException
}
